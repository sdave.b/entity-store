/* Properties.hpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "NamedType/named_type.hpp"

#include <optional>
#include <string>

#include "Types.hpp"

namespace EntityStore
{
	using fluent::NamedType;
	using namespace Property;

	struct Properties
	{
		explicit Properties(Title title);


		explicit Properties(std::optional<Title> title,
						  std::optional<Description> description = std::nullopt,
							std::optional<TimeStamp> timestamp = std::nullopt);

		bool operator==(const Properties&) const;

		std::optional<Title>       title;
		std::optional<Description> description;
		std::optional<TimeStamp>   timestamp;
	};
}

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=4 sts=0 sw=8 autoindent smartindent cindent noexpandtab ft=cpp.doxygen :
