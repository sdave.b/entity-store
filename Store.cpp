/* Store.hpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 *
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Store.hpp"
#include "StoreOperationException.hpp"

using fluent::NamedType;
using EntityStore::Properties;
using namespace EntityStore;

Store& Store::insert(const Id& id, const Properties& properties)
{
	// emplace returns a std::pair object
	// pair.first an iterator to the object
	// pair.second is the operation result
	auto emplaceResult =
		todos.emplace(id.get(), Todo(id, properties)).second;

	if (!emplaceResult)
	{
		throw StoreOperationException(OperationType::Insert);
	}

	return *this;
}

Store& Store::update(const Id& id, const Properties& properties)
{
	try
	{
		auto& existingRecord = todos.at(id.get());

		auto possibleTitle = properties.title;
		auto possibleDesc = properties.description;
		auto possibleTimeStamp = properties.timestamp;

		if (possibleTitle)
		{
			existingRecord.title = possibleTitle.value().get();
			titles[existingRecord.title] = id.get();
		}

		if (possibleDesc)
		{
			existingRecord.description = possibleDesc.value().get();
			descriptions[existingRecord.description] = id.get();
		}

		if (possibleTimeStamp)
		{
			existingRecord.timestamp =
			                        possibleTimeStamp.value().get();
			timestamps[existingRecord.timestamp] = id.get();
		}

	}
	catch (const std::out_of_range& exception)
	{
		auto errorMsg =
		    "the record you are trying to update likely does not exist";

		throw StoreOperationException(OperationType::Update, errorMsg);
	}
	catch (const std::exception& exception)
	{
		throw StoreOperationException(OperationType::Update);
	}

	return *this;
}

Store& Store::remove(const Id& id)
{
	todos.erase(id.get());
	return *this;
}

Properties Store::get(const Id& id) const
{
	try
	{
		auto record = todos.at(id.get());
		auto retval = Properties(Title(record.title));

		retval.description = Description(record.description);
		retval.timestamp = TimeStamp(record.timestamp);

		return retval;
	}
	catch (const std::out_of_range& exception)
	{
		auto errorMsg =
		    "the record you are trying to update likely does not exist";

		throw StoreOperationException(OperationType::Get, errorMsg);
	}

	return Properties(Title("Empty Task"));
}

const std::optional<std::list<Id> >
Store::query(std::variant<Title, Description, TimeStamp> parameter) const
{
	return std::nullopt;
}

const std::optional<std::list<Id> >
Store::range_query(std::any range) const
{
	return std::nullopt;
}
