/* Store.hpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 *
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "NamedType/named_type.hpp"

#include "Todo.hpp"
#include "Properties.hpp"
#include "Types.hpp"
#include "StoreOperationException.hpp"

#include <cstdint>

#include <map>
#include <unordered_map>
#include <list>
#include <optional>
#include <variant>
#include <any>

using fluent::NamedType;
using EntityStore::Properties;

namespace EntityStore
{
	using QueryResult = std::optional<std::list<Id> >;
	class Store
	{
	public:

		Store& insert(const Id& id, const Properties& properties);
		Store& update(const Id& id, const Properties& properties);
		Store& remove(const Id& test);

		Properties get(const Id& id) const;

		const std::optional<std::list<Id> >
		query(std::variant<Title, Description, TimeStamp> parameter)
		                                                         const;
		const std::optional<std::list<Id> >
		range_query(std::any range) const;
	protected:
		// Chosen for quick lookup speeds by ID.
		std::unordered_map<int64_t, Todo> todos;

		// Chosen for their ease of insert/update/remove speed
		std::map<std::string, int64_t> titles;
		std::map<std::string, int64_t> descriptions;
		std::map<double, int64_t>      timestamps;
	};
};

