/* StoreInspector.cpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "Store.hpp"
#include "Properties.hpp"

#include <string>

namespace EntityStore
{

	using todo_map_t = std::unordered_map<int64_t, Todo>;
	using title_map_t = std::map<std::string, int64_t>;
	using desc_map_t = std::map<std::string, int64_t>;
	using timestamp_map_t = std::map<double, int64_t>;

	struct StoreInspector : public Store
	{
		StoreInspector() = delete;
		StoreInspector(const Store& ref) : Store(ref) {}

		static bool
		are_same_object(const Store& first, const Store& second)
		{
			return &first == &second;
		}

		static Properties
		extract_properties_from_todo(const Todo& todo)
		{
			Properties result(Title(todo.title));
			result.description = Description(todo.description);
			result.timestamp = TimeStamp(todo.timestamp);

			return result;
		}

		static todo_map_t get_todo_map(const Store& store)
		{
			auto instance = StoreInspector(store);
			return instance.todos;
		}

		static title_map_t get_titles_map(Store& store)
		{
			auto instance = StoreInspector(store);
			return instance.titles;
		}

		static desc_map_t get_desc_map(Store& store)
		{
			auto instance = StoreInspector(store);
			return instance.descriptions;
		}

		static timestamp_map_t get_timestamp_map(Store& store)
		{
			auto instance = StoreInspector(store);
			return instance.timestamps;
		}
	};
}
