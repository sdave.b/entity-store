/* main.cpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Unit-testing framework https://is.gd/53LpGQ */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <exception>
#include <functional>
#include <typeinfo>

#include "Store.hpp"
#include "Properties.hpp"
#include "StoreInspector.hpp"

using namespace std;
using namespace EntityStore;
using namespace EntityStore::Property;

TEST_CASE("assignment 1", "[assignment 1]")
{
	Store store;

	// Insert a new entity
	store.insert(Id(2133),
	  Properties(Title("Buy Milk"),
			 Description("made of almonds!"),
			 TimeStamp(2392348.12233)));

	// Update only specified properties
	store.update(Id(2133),
		   Properties(Title("Buy Chocolate")));

	// Retrieve properties of an entity
	auto properties = store.get(Id(2133));

	// Remove an entity with given id
	store.remove(Id(2133));
}


TEST_CASE("assignment 2", "[assignment 2]")
{
	Store store;

	// Insert a new entity
	store.insert(Id(2133),
	  Properties(Title("Buy Milk"),
			 Description("made of almonds!"),
			 TimeStamp(2392348.12233)));


	// returns a set of ids where the value of the "title" property is equal to "Buy Milk"
	store.query(Title("Buy Milk"));


	// returns a set of ids where the value of the "timestamp" property is in the given range
	store.range_query(Range<TimeStamp>(1000, 1300));
}

TEST_CASE("Assignment 2", "[test_2]")
{
}

TEST_CASE("Assignment 3", "[test_3]")
{

}

TEST_CASE("Assignment 4", "[test_4]")
{

}


/* My unit testsing */

TEST_CASE("Test 1", "[test_1]")
{
	Store store;

	auto validId = Id(2133);
	auto invalidId = Id(-1);

	auto insertionTitle = Title("Buy Milk");
	auto insertionDescription = Description("made of almonds!");
	auto insertionTimeStamp = TimeStamp(2392348.12233);

	auto insertionProperties =  Properties(insertionTitle,
	                                 insertionDescription,
	                                 insertionTimeStamp);

	// Insert a new entity
	auto result = std::ref(store.insert(validId, insertionProperties));
	REQUIRE(StoreInspector::are_same_object(result.get(), store));

	auto todo = StoreInspector::get_todo_map(store).at(validId.get());
	REQUIRE(todo.id == validId.get());
	REQUIRE(todo.title == insertionTitle.get());
	REQUIRE(todo.description == insertionDescription.get());
	REQUIRE(todo.timestamp == insertionTimeStamp.get());

	REQUIRE_THROWS_AS(store.insert(validId, Properties(Title("Buy chocolate"))),
	                  StoreOperationException);

	// Update only specified properties
	result = std::ref(store.update(validId, Properties(Title("Buy chocolate"))));
	REQUIRE(StoreInspector::are_same_object(result.get(), store));

	todo = StoreInspector::get_todo_map(store).at(validId.get());
	REQUIRE(todo.id == validId.get());
	REQUIRE(todo.title == "Buy chocolate");
	REQUIRE(todo.description == insertionDescription.get());
	REQUIRE(todo.timestamp == insertionTimeStamp.get());

	// Retrieve properties of an entity
	auto properties = store.get(validId);

	todo = StoreInspector::get_todo_map(store).at(validId.get());
	auto expected_properties = StoreInspector::extract_properties_from_todo(todo);

	REQUIRE(properties.title->get() == expected_properties.title->get());
	REQUIRE(properties.description->get() == expected_properties.description->get());
	REQUIRE(properties.timestamp->get() == expected_properties.timestamp->get());

	REQUIRE_THROWS_AS(store.get(invalidId), StoreOperationException);

	// Remove an entity with given id
	store.remove(validId);
	REQUIRE_THROWS_AS(
			StoreInspector::get_todo_map(store).at(validId.get()),
			std::out_of_range);

}

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=4 sts=0 sw=8 autoindent smartindent cindent noexpandtab ft=cpp.doxygen :
