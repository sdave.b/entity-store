# Feel free to edit this as needed
#
program = main
sources = Store.cpp Properties.cpp main.cpp
objects = $(patsubst %.cpp, obj/%.o, $(sources))
deps = *.hpp NamedType/*.hpp

CXXFLAGS = -std=c++17 -Wall

all: $(program)

$(program): $(objects)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS)  -o $@ $(objects)

obj/%.o : %.cpp $(deps)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

run: $(program)
	./$(program)

# To help with language server protocol completion using vim-async-lsp
compile_commands.json: clean $(sources)
	compiledb make $(program)

.PHONY: clean

clean:
	$(RM) $(program) $(objects)

rebuild:  compile_commands.json run
