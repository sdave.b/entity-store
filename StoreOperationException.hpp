/* StoreOperationException.hpp
 * Submission for Cultured Code's Data Persistance and Sync Engineer
 *
 * Copyright © 2020 Saul D. Beniquez
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <string>
#include <sstream>
#include <stdexcept>
#include <optional>
#include <functional>


namespace EntityStore
{
	enum OperationType
	{
		Insert = 0,
		Update,
		Remove,
		Get
	};

	template<typename T>
	using reference = std::optional<std::reference_wrapper<T> >;

	class StoreOperationException: public std::runtime_error
	{
	public:

		explicit
		StoreOperationException(const OperationType op,
		                        const std::optional<std::string>
						additionalData = std::nullopt)
		: std::runtime_error("StorageOperationException")
		{
			auto opString = this->getOperationString(op);
			auto buffer = std::stringstream();

			buffer << "StoreOperationException: "
			       << opString << " operation failed!";

			if (additionalData.has_value())
			{
				buffer << std::endl
				       << additionalData.value() << std::flush;
			}
			this->message = buffer.str();

		}


		virtual const char* what() const throw() override
		{
			return message.c_str();
		}

	private:
		static const std::string getOperationString(OperationType op)
		{
			std::string retval;

			switch (op)
			{
			case Insert:
				retval = "Insert";
				break;
			case Update:
				retval = "Update";
				break;
			case Remove:
				retval = "Remove";
				break;
			case Get:
				retval = "Get";
				break;
			default:
				retval = "Unknown";
				break;
			}

			return retval.c_str();
		}

		std::string message;
	};
}
